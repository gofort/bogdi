# Bogdi Blog

![ci](https://github.com/gofort/bogdi/actions/workflows/ci.yml/badge.svg)
![uptime](https://badgen.net/uptime-robot/month/m791753153-c6291fad3705be6ac409a645)

This is source code of my blog.

## Tech stack of this blog

* Hugo
* Nginx
* GitHub Actions
* Github Packages
* G-Core Labs Cloud (3x Virtual Machines)
  * Consul
  * Nomad
  * Traefik
  * Docker
* G-Core Labs DNS
* G-Core Labs CDN
